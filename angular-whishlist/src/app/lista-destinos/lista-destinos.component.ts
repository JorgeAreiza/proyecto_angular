import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinosApiClient } from '../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  constructor(public DestinosApiClient: DestinosApiClient) {
    
  }

  ngOnInit(): void {
  }

  Agregado(D: DestinoViaje){
    this.DestinosApiClient.add(D);
    this.onItemAdded.emit(D);
  }

  Elegido(D: DestinoViaje){
    this.DestinosApiClient.getAll().forEach(X => X.setSelected(false));
    D.setSelected(true);
  }

}
