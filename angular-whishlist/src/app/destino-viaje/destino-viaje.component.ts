import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() Destino: DestinoViaje;
  @Input() Position: number;
  @HostBinding('attr.class')cssClass = 'col-md-4 col-lg-4';
  @Output() Clicked: EventEmitter<DestinoViaje>;

  constructor() {
    this.Clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  Ir(){
    this.Clicked.emit(this.Destino);
    return false;
  }

}
