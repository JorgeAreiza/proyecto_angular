import { v4 as uuid } from 'uuid';

export class DestinoViaje{
    private Selected: boolean;
    public Servicios: string[];

    constructor(public Nombre:string, public ImagenURL:string){
        this.Servicios = ['Desayuno', 'Almuerzo', 'Comida']
    }
    isSelected(): Boolean{
        return this.Selected;
    }
    setSelected(Selected: Boolean){
        this.Selected = true;
    }
}